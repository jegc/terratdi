# terratdi

TDI terraform example

```shell
cd tests/unit
bundle install --path vendor/bundle
bundle exec inspec exec .
```

Write code

```
terraform fmt
terraform init
terraform validate
terraform plan -out=terraform.tfstate
terraform show -json terraform.tfstate > tfstate.json
cd tests/unit
bundle exec inspec exec .
```

```
cd tests/integration
go test -v -run TestVpcCidrBlock
```
