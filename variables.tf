variable "aws_profile" {
  default = "default"
}

variable "aws_region" {
  default = "us-west-2"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "ami" {
  default = "ami-070fdf86f8679add0"
}
