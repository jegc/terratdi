# copyright: 2019,  Jorge Ernesto Guevara Cuenca

title "Subnet name test"

describe json({ command: "jq '.resource_changes[] | select (.type==\"aws_subnet\")' ../../tfstate.json || echo {}"}) do
  its('name') { should eq 'subnet' }
end
