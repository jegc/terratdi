# copyright: 2019,  Jorge Ernesto Guevara Cuenca

title "VPC name test"

describe json({ command: "jq '.resource_changes[] | select (.type==\"aws_vpc\")' ../../tfstate.json || echo {}"}) do
  its('name') { should eq 'vpc' }
end
