# copyright: 2019,  Jorge Ernesto Guevara Cuenca

title "Instance tests"

describe json({ command: "jq '.resource_changes[] | select (.type==\"aws_instance\")' ../../tfstate.json || echo {}"}) do
  its(['change','after','ami']) { should eq 'ami-b374d5a5' }
  its(['change','after','instance_type']) { should eq 't2.micro' }
end
